using Agate.MVC.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

namespace ExampleGame.Module.ClickGame
{
    public class ClickGameView : ObjectView<IClickGameModel>
    {
        [SerializeField] private TextMeshProUGUI _cointText;
        [SerializeField] private Button _earnCoinButton;
        [SerializeField] private Button _spendCoinButton;
        [SerializeField] private Button _backButton;

        protected override void InitRenderModel(IClickGameModel model)
        {
            _cointText.text = $"Coin: {model.Coin.ToString()}";
        }

        protected override void UpdateRenderModel(IClickGameModel model)
        {
            _cointText.text = $"Coin: {model.Coin.ToString()}";
        }

        public void SetCallbacks(UnityAction OnClickEarnCoin, UnityAction OnClickSpendCoin, UnityAction OnClickBack)
        {
            _earnCoinButton.onClick.RemoveAllListeners();
            _spendCoinButton.onClick.RemoveAllListeners();
            _backButton.onClick.RemoveAllListeners();
            _earnCoinButton.onClick.AddListener(OnClickEarnCoin);
            _spendCoinButton.onClick.AddListener(OnClickSpendCoin);
            _backButton.onClick.AddListener(OnClickBack);
        }

        
    }

}
